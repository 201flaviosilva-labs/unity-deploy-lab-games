import Assets from "./Assets.js";
import Games from "./games.js";
import { randomColor } from "./utils.js";


main();
function main() {
	const timeSeconds = 10;
	updateTitleColor();
	setInterval(updateTitleColor, timeSeconds * 1000);

	const listUL = document.getElementById("list");
	Games.map(g => {
		const li = document.createElement("li");
		const h2 = document.createElement("h2");
		const div = document.createElement("div");

		const links = Object.keys(g);
		for (let i = 0; i < links.length; i++) {
			if (links[i] == "name") continue;

			const a = document.createElement("a");
			const img = document.createElement("img");
			img.src = Assets[links[i]];

			a.title = links[i];
			a.href = g[links[i]];

			a.appendChild(img);
			div.appendChild(a);
		}

		h2.innerHTML = g.name;

		li.appendChild(h2);
		li.appendChild(div);

		listUL.appendChild(li);
	});
}

function updateTitleColor() {
	document.getElementById("titleUnity").style.color = randomColor();
	document.getElementById("titleFixolas").style.color = randomColor();
	document.getElementById("footerCode").style.color = randomColor();
}
