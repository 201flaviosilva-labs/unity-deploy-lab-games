// <script src="./script.js" type="module"></script>
// import Assets from "../../Assets.js";

const commitCode = "4f36e7f10d8f5e79d54298bccccfa2166f37ec7b";
export const assetsPath = "https://bitbucket.org/201flaviosilva/assets/raw/" + commitCode;


const Assets = {
	Apple: assetsPath + "/Icons/Apple.svg",
	Code: assetsPath + "/Icons/Code.svg",
	Linux: assetsPath + "/Icons/Linux.svg",
	Web: assetsPath + "/Icons/Web.svg",
	Windows: assetsPath + "/Icons/Windows.svg",
};

export default Assets;
