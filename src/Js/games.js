const Games = [
	{
		name: "2D Labs",
		Web: "./src/Games/2DLabs/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/unity-2d-labs/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/unity-2d-labs",
	},
	{
		name: "Breakout",
		Web: "./src/Games/Breakout/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/breakout-unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/breakout-unity/",
	},
	{
		name: "Cube Run",
		Web: "./src/Games/CubeRun/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/cube-run-unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/cube-run-unity/",
	},
	{
		name: "Delivery Driver",
		Web: "./src/Games/DeliveryDriver/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/delivery-driver-unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/delivery-driver-unity/",
	},
	{
		name: "Infinity Down",
		Web: "./src/Games/InfinityDown/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/infinity-down-unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/infinity-down-unity/",
	},
	{
		name: "Laser Defender",
		Web: "./src/Games/LaserDefender/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/laser-defender-unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/laser-defender-unity/",
	},
	{
		name: "Pong",
		Web: "./src/Games/Pong/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/Pong-Unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/Pong-Unity/",
	},
	{
		name: "Quiz Master",
		Web: "./src/Games/QuizMaster/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/quiz-master-unitydownloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/quiz-master-unity",
	},
	{
		name: "Snake",
		Web: "./src/Games/Snake/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/Snake-Unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/Snake-Unity/",
	},
	{
		name: "Snow Boarder",
		Web: "./src/Games/SnowBoarder/index.html",
		Windows: "https://bitbucket.org/201flaviosilva-labs/Snow-Boarder-Unity/downloads/Windows.zip",
		Code: "https://bitbucket.org/201flaviosilva-labs/Snow-Boarder-Unity/",
	},
	{
		name: "Unity Play",
		Web: "https://play.unity.com/u/MeiaGaspea",
	},
	{
		name: "Itch io",
		Web: "https://meiagaspea.itch.io/",
	},
];

export default Games;
